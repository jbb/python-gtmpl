# Go Template Engine for python

Instead of wrapping the go builtin `text/template`, this project is based on the 
[gtmpl](https://crates.io/crates/gtmpl) rust create.

# Usage

Currently the module only exposes one function.

```
import gtmpl
gtmpl.render_template(template: str, **kwargs)
```

# Example

``` 
import gtmpl
gtmpl.render_template("Hello {{ .planet }}", planet="Earth")
```

# Installation

```
pip3 install maturin

maturin build
pip3 install path/too/generated/.whl
```

# Development setup

```
pip3 install maturin virtualenv
virtualenv venv
source venv/bin/activate

maturin develop
```
