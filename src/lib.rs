/*
    Copyright (C) 2019 Jonah Brüchert <jbb@kaidan.im>
*/

use pyo3::prelude::{pyfunction, pymodule, PyModule, PyResult, Python, PyAnyMethods, PyDictMethods, PyModuleMethods};
use pyo3::exceptions::PyValueError;
use pyo3::types::{PyDict, PyAny, PyString, PyInt, PyFloat};
use pyo3::{wrap_pyfunction, Bound};
use std::collections::HashMap;

use ::gtmpl as gtmpl_rust;
use ::gtmpl::Value;

fn py_any_to_gtmpl_value(value: &Bound<PyAny>) -> PyResult<Value> {
    if let Ok(dict) = value.downcast::<PyDict>() {
        let mut map = HashMap::<String, Value>::new();
        for key in dict.keys() {
            map.insert(key.to_string(), py_any_to_gtmpl_value(&dict.get_item(key)?.unwrap())?);
        }
        Ok(Value::Map(map))
    } else if let Ok(string) = value.downcast::<PyString>() {
        Ok(Value::String(string.to_string()))
    } else if let Ok(num) = value.downcast::<PyInt>() {
        Ok(Value::Number(num.extract::<i64>().unwrap().into()))
    } else if let Ok(num) = value.downcast::<PyFloat>() {
        Ok(Value::Number(num.extract::<f64>().unwrap().into()))
    } else {
        Ok(Value::String(value.to_string()))
    }
}

#[pyfunction]
#[pyo3(signature = (template, **kwds))]
fn render_template(template: &str, kwds: Option<&Bound<PyDict>>) -> PyResult<String> {
    let map = if let Some(args) = kwds {
        py_any_to_gtmpl_value(args.as_any())?
    } else {
        Value::NoValue
    };
    let output  = gtmpl_rust::template(template, map);

    match output {
        Ok(ref text) => Ok(text.to_string()),
        Err(e) => Err(PyValueError::new_err(e.to_string()))
    }
}

/// This module is a python module implemented in Rust.
#[pymodule]
fn gtmpl(_py: Python, m: &Bound<PyModule>) -> PyResult<()> {
    return m.add_wrapped(wrap_pyfunction!(render_template));
}
