#!/usr/bin/env python3

import gtmpl

file = open("example.txt")
print(gtmpl.render_template(file.read(), title="Usage Example", description="This is an example using the go templating syntax from python", content="And it seems to work"))
